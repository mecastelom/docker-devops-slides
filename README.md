## Docker File
### Step 1: Running from official NGINX image
```sh
docker run -p 8080:80 -v /Users/matt/Sites/docker-devops-slides/html:/usr/share/nginx/html:ro nginx
```
### Step 2: Building Custom Docker Image
```sh
docker build -t test-nginx .

docker run -p 8080:80 test-nginx
```